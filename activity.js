db.hotelRoom.insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_avialable: "10",
	isAvailable: "false"
});

db.hotelRoom.insertMany([
		{
			name: "double",
			accomodates: 3,
			price: 2000,
			description: "A room fit for a small family going on a vacation",
			rooms_avialable: "5",
			isAvailable: "false"
		},

		{
			name: "queen",
			accomodates: 4,
			price: 4000,
			description: "A room with a queen sized bed perfect for a simple getaway",
			rooms_avialable: "15",
			isAvailable: "false"
		}


	]);



//find room
db.hotelRoom.find();

//find the name double
db.hotelRoom.find({name: "double"});

// updateOne queen room

db.hotelRoom.updateOne(
	{name: "queen"},
	{
		$set: {
			rooms_avialable: "0"
		}
	}
);

// delete all room that have 0 rooms available
db.hotelRoom.deleteMany({rooms_avialable: "0"});